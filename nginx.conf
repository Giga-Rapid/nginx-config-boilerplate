# Nginx main configuration file

user nginx;
pid /var/run/nginx.pid;
error_log /var/log/nginx/error.log warn;
pcre_jit on;
worker_processes auto;
worker_rlimit_nofile 8192;
worker_shutdown_timeout 20s;

# Load dynamic modules
include conf.d/modules.conf;

events {
	worker_connections 4096;
}

http {
	# Map file name extensions to file types
	include conf.d/mime.types;
	# Include access_log formats
	include conf.d/log.formats;
	# Include default reverse proxy configuration 
	include conf.d/proxy.conf;
	# Include default fastcgi configuration
	include conf.d/fastcgi.conf;
	# Include gzip compression configuration
	include conf.d/gzip.conf;
	# Caching expiration based on MIME types
	include conf.d/cache.conf;
	# Improve security by adding headers
	include conf.d/security_headers.conf;

	# Configure logging
	access_log /var/log/nginx/access.log main;

	# Default HTTP content-type
	default_type application/octet-stream;

	# Optimizations
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	reset_timedout_connection on;
	keepalive_timeout 20;
	server_names_hash_bucket_size 128;
	types_hash_max_size 2048;

	# Tweak nginx's core module
	server_tokens off;
	charset utf-8;
	client_header_timeout 30s;
	client_body_timeout 30s;
	client_max_body_size 20M;

	# Index configuration
	index index.html index.htm index.php;
	autoindex off;

	# Auto include extra configurations
	include conf.d/*.econf;
	# Include virtualhosts
	include vhosts.d/*.conf;
}